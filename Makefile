prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = programa.cpp colisionador.cpp numeros.cpp lista.cpp
OBJ = programa.o colisionador.o numeros.o lista.o
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
