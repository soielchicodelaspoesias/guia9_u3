#include <iostream>
#include "Colisionador.h"

using namespace std;
// se instancia el constructor
Colisionador::Colisionador() {}

// funcion hash
 int Colisionador::hash_normal(int numero){
   // se obtiene la posicion
   int d = ((numero)%20);
   // se retorna el numero
   return d;
 }
 // funcion H'()
 int Colisionador::hash_prima(int numero){
   // se obtiene la direccion con numero +1
   int d = ((numero+1)%20);
   // se retorna el numero
   return d;
 }
// funcion que verifica si hay coliciones y sino agrega el numero el la posicion designada segun la funcion hash
void Colisionador::agrega_hash(int *arreglo, int numero, char letra){
  // se inicia una variable
  int p;
  // se determina la posicion con la funcion hash
  p = hash_normal(numero);
  // si la letra que ingreso es una l se trabaja con prueba lineal
  if(letra == 'L'){
    // si arreglo en posicion es distinto de '\0' y ademas hay otro numero en esa posicion
    if((arreglo[p] != '\0') && arreglo[p] != numero){
      // hay una colision
      cout << "Colision" << endl;
      // se llama a prueba lineal para soluconar la colision
      prueba_lineal(arreglo, numero);
    }else{
      // sino se agrega el numero e imprime la posicion en la que esta
      cout << "El numero se encuentra en la posicion: " << p << endl;
      arreglo[p] = numero;
    }
  }
  // si la letra que ingreso es una C se trabaja con prueba cuadratica
  if(letra == 'C'){
      // si arreglo en posicion es distinto de '\0' y ademas hay otro numero en esa posicion
    if((arreglo[p] != '\0') && arreglo[p] != numero){
      // se llama a prueba cuadratica para soluconar la colision
      cout << "Colision" << endl;
      prueba_cuadratica(arreglo, numero);
    }else{
      // sino se agrega a la lista y se imprime a en la posicion
      arreglo[p] = numero;
      cout << "El numero se encuentra en la posicion: " << p << endl;
    }
  }
  // si la letra que ingreso es una D se trabaja con doble direccion hash
  if(letra == 'D'){
    // si arreglo en posicion es distinto de '\0' y ademas hay otro numero en esa posicion
    if((arreglo[p] != '\0') && arreglo[p] != numero){
      // se llama a prueba doble direccion hash para soluconar la colision
      cout << "Colision" << endl;
      doble_direccion_hash(arreglo, numero);
    }else{
      // sino se agrega a la lista y se imprime a en la posicion
      arreglo[p] = numero;
      cout << "El numero se encuentra en la posicion: " << p << endl;
    }
  }
}
// funcion que imprime la lista
void Colisionador::imprime(int *arreglo){
  for(int i=0; i<20;i++){
    cout << "|" <<  arreglo[i]<< "|" << " ";
  }
  cout << endl;
}
// funcion que soluciona las colisiones
void Colisionador::doble_direccion_hash(int *arreglo, int numero){
  // se crean
  int d, dx;
  // se genera la posicion del numero
  d = hash_normal(numero);;
  // si en arreglo en la posicion d es distinto de '\0' y ademas es igual a l numero que se desea agregar
  if((arreglo[d] != '\0') && (arreglo[d] == numero)){
    // se muestra en pantalla que es numero ya esta y la posicion
    cout << "El numero esta en la posicion: " << d << endl;
  // sino es necesario buscarle una nueva posicion al numero
  }else{
    // se genera una nueva posicion
    dx = hash_prima(d);
    // y se va recorriendo las posiciones siguientes del arreglo para encontrar una posicion correcta
    while((dx <= 20) && (arreglo[dx] != '\0') && (arreglo[dx] != numero) && (dx != d)){
        // se va calculando denuevo una nueva direccion hasta que cumpla con las condiciones
        dx = hash_prima(dx);
    }
    // una vez superado el ciclo quiere decir que se encontro una posiciones
    // se agrega el numero es esa posicion
    arreglo[dx] = numero;
    // se imprime la posicion en la que se encontro el numero
    cout << "El numero se encuentra en la posicion: " << dx << endl;
  }
}
// funcion que resuelve las colisiones con prueba cuadratica
void Colisionador::prueba_cuadratica(int *arreglo, int numero){
  // se crean las variables
  int d, dx, i;
  // se genera la posicion en la que deberia ir el numero a agregar
  d = hash_normal(numero);
  // si el arreglo en esa posicion es distinto de '\0' y es igual a numero
  // quiere decir que el numero ya esta en el arreglo
  if((arreglo[d] != '\0') && arreglo[d] == numero){
    // por lo que se imprime la posicion en la que se encuentra
    cout << "El numero esta en la posicion: " << d << endl;
  // en cambio si hay otro numero en esa posicion hay una colision
  }else{
    // se iguala i a 1
    i = 1;
    // se genera una nueva posicion
    dx = (d + (i*i));
    // se recorren las posiciones del arreglo hasta que se cumpla la condicion
    while((arreglo[dx] != '\0') && (arreglo[dx] != numero)){
      // se le va sumando 1 a i
      i = i+1;
      // se cambia la posicion dependiendo de i
      dx = (d+(i*i));
      // si dx se sale del arreglo
      if(dx > 21){
        // se reinician las variables
        i = 0;
        dx = 1;
        d = 1;
      }
    }
    // finalmente cuando sale del ciclo se encuentra una posicion por lo que se guarda el numero en esa posicion del arreglo
    arreglo[dx] = numero;
    // se imprime la posicion en la que se agrego
    cout << "El numero se encuentra en la posicion: " << dx << endl;
  }
}
// funcion que resuelve las colisiones con prueba lineal
void Colisionador::prueba_lineal(int *arreglo, int numero){
  // se incian las variables
  int d, dx;
  // se obtiene la posicion en la que debe ir el numero
  d = hash_normal(numero);
  // se comprueba si el numero ya esta en el arreglo
  if((arreglo[d] != '\0') && arreglo[d] == numero){
    // si esta se imprime que ya esta y la posicion
    cout << "El numero esta en la posicion: " << d << endl;
  }else{
    // encambio quiere decir que hay una colision
    // se le suma uno a la posicion
    dx = d+1;
    // se van recorrinedo las posiciones del areglo hasta encontrar una vacia
    while( (dx<=20) && (arreglo[dx] != '\0') && (arreglo[dx] != numero) && (dx != d)){
      // se le va sumando 1 a la posicion
      dx = dx + 1;
      // si la poscion sale del arreglo
      if(dx == 21){
        // se reinicia la posicion
        dx = 1;
      }
    }
    // un vez fuera del ciclo quiere decir que se encontro una posicion y se guarda el numero en esa posicion
    arreglo[dx] = numero;
    // se imprime la posicion en la que se encontro
    cout << "El numero se encuentra en la posicion: " << dx << endl;
  }

}
