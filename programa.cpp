#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Colisionador.h"
#include "Programa.h"
#include "Numeros.h"
#include "Lista.h"
using namespace std;
// se crea la clase programa
class Programa {
    private:
        // metodos privados
        Lista *lista = NULL;
    public:
        // generar la lista
        Programa() {
            this->lista = new Lista();
        }
        // se obtiene la lista
        Lista *get_lista() {
            return this->lista;
        }
};
// funcion que cumprueba que la lista esta llena
int comprobar(int *arreglo){
  int comp = 0;
  //se recorre la lista
  for(int i=0; i<20;i++){
    // si el arreglo en la posicion i es igual a un espacio
    if(arreglo[i] == '\0'){
      // se le suma a comp
      comp ++;
    }
  }
  // se retorna comp
  return comp;
}
// funcion del menu
int Menu() {
  int op;
  // imprime las opciones
  do {
    cout << "____________" << endl;
    cout << "1) Insertar" << endl;
    cout << "2) Buscar" << endl;
    cout << "0) Salir" << endl;
    // le pide la opcion al usuario
    cout << "Opción: ";
    cin >> op;
  } while (op<0 || op>2);
  // retorna la opcion
  return (op);
}
// funcion que busca dentro del arreglo
void buscador(int *arreglo, int buscar){
  // se crea un comprobador
  int comprobador = 0;
  // se recorre la lista
  for(int i=0; i<20;i++){
    // si el arreglo en la posicion i es igual al numero
    if(arreglo[i] == buscar){
      // se le suma al comprobador
      comprobador++;
      // se imprime la posicion del numero
      cout << "El numero esta en la posicion: " << i << endl;
      }
  }
  // si el comprobador es igual a 0 el numero no esta en la lista
  if(comprobador == 0){
    cout << "El numero esta no esta en la lista" << endl;
  }
}
// se inicia la lista con NULL
void iniciar_lista(int *arreglo){
  for(int i=0; i<20;i++){
    arreglo[i] = '\0';
  }
}

int main(int argc, char *argv[]) {
    // se obtiene la letra ingresada por terminal
    char* metodo = argv[1];
    // se inicia el arreglo
    int arreglo[20];
    iniciar_lista(arreglo);
    // condicion por si se ingresa una opcion incorrecta
    int condicion = 0;
    int opcion;
    int numero;
    int comp;
    int buscar;
    //se crea la lista
    Programa p = Programa();
    Lista *lista = p.get_lista();
    // se llena de '\0'
    for(int i=0; i<20;i++){
      lista->crear(new Numeros('\0'));
    }
    // se instancia la clase
    Colisionador c = Colisionador();
    char letra = metodo[0];
    // si la letra ingresada por terminal es l
    if(metodo[0] == 'L'){
      condicion++;
      // se utiliza la prueba lineal
      cout << "PRUEBA LINEAL" << endl;
      opcion = Menu();

      while (opcion) {

        switch(opcion) {
          // si el caso 1
          case 1:
            // se comprueba que la lista no este llena
            comp = comprobar(arreglo);
            if(comp != 0){
              // se le pide un numero
              cout << "Inserte un numero: ";
              cin >> numero;
              // se llama a agregar
              c.agrega_hash(arreglo, numero, letra);
              // se imprime la lista
              c.imprime(arreglo);
            }else{
              // sino la lista esta llena y se muestra en pantalla
              cout << "La lista esta llena" << endl;
            }
            break;

          case 2:
            // se le pide al usuario el numero por buscar
            cout << "Inserte el numero a buscar: ";
            cin >> buscar;
            // y se llama a la funcion que buscar
            buscador(arreglo, buscar);
            break;

          case 0:
            // si la opcion es 0 se cierra el programa
            exit(0);
        }

        opcion = Menu();
      }

    }
    // si la letra que ingresa es una C
    if(metodo[0] == 'C'){
      condicion++;
      // se trabaja con prueba cuadratica
      cout << "PRUEBA CUADRATICA" << endl;
      opcion = Menu();

      while (opcion) {

        switch(opcion) {
          case 1:
            // se comprueba que la lista no este llena
            comp = comprobar(arreglo);
            if(comp != 0){
              // se le pide un numero al usuario
              cout << "Inserte un numero: ";
              cin >> numero;
              // se llama a agregar
              c.agrega_hash(arreglo, numero, letra);
              // se imprime la lista
              c.imprime(arreglo);
            }else{
              // si la lista esta llena lo muestra en pantalla
              cout << "La lista esta llena" << endl;
            }
            break;

          case 2:
            // si la opcion es 2 le pide un numero a buscar y la llama a la funcion
            cout << "Inserte el numero a buscar: ";
            cin >> buscar;
            buscador(arreglo, buscar);
            break;

          case 0:
            // si es 0 se cierra el programa
            exit(0);
        }

        opcion = Menu();
      }

    }
    // si la letra que ingresa por terminal es una D se trabaja con doble direccion hash
    if(metodo[0] == 'D'){
      condicion ++;
      cout << "DOBLE DIRECCION HASH" << endl;
      opcion = Menu();

      while (opcion) {

        switch(opcion) {
          // si el caso es uno se ingresa un numero
          case 1:
            // se comprueba si la lista esta llena
            comp = comprobar(arreglo);
            if(comp != 0){
              cout << "Inserte un numero: ";
              cin >> numero;
              // se llama a la funcion agregar
              c.agrega_hash(arreglo, numero, letra);
              // se imprime la lista
              c.imprime(arreglo);
            }else{
              // si esta llena no se pueden agregar numeros
              cout << "La lista esta llena" << endl;
            }
            break;
          // si el caso es 2 se busca un numero
          case 2:
            // se ingresa un numero
            cout << "Inserte el numero a buscar: ";
            cin >> buscar;
            // se llama a la funcion buscar
            buscador(arreglo, buscar);
            break;
          // si el caso es 0 se cierra el programa
          case 0:
            exit(0);
        }

        opcion = Menu();
      }

    }
    if(metodo[0] == 'E'){
      condicion ++;
      // se utiliza la prueba lineal
      cout << "ENCADENAMIENTO" << endl;
      opcion = Menu();

      while (opcion) {

        switch(opcion) {
          // si el caso 1
          case 1:
              // se le pide un numero
              cout << "Inserte un numero: ";
              cin >> numero;
              lista->agrega_hash_lista_enlazada(lista, numero);
              lista->imprimir();
            break;

          case 2:
            // se le pide al usuario el numero por buscar
            cout << "Inserte el numero a buscar: ";
            cin >> buscar;
            // y se llama a la funcion que buscar
            buscador(arreglo, buscar);
            break;

          case 0:
            // si la opcion es 0 se cierra el programa
            exit(0);
        }

        opcion = Menu();
      }

    }
    // si la opcion ingresada no es correcta
    if(condicion == 0){
      cout << "La opcion ingresada no es valida" << endl;
      cout << "Recuerde que la letra tiene que ser: " << endl;
      cout << "-L:{Prueba lineal}" << endl;
      cout << "-C:{Prueba cuadratica}" << endl;
      cout << "-D:{Prueba doble hash}" << endl;
      cout << "-E:{Encadenamiento}" << endl;

    }
  return 0;
}
