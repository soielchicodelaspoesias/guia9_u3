#include "Numeros.h"

// se define el nodo como estructura
typedef struct _Nodo {
    // con los campos numeros y sig
    Numeros *numeros;
    struct _Nodo *sig;
} Nodo;
