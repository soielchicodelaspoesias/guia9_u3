#include <iostream>
using namespace std;

#ifndef NUMEROS_H
#define NUMEROS_H
// clase
class Numeros {
    // metodos privados
    private:
        int numero = 0;

    public:
      //funciones
      void set_numero(int numero);
      Numeros(int numero);
      int get_numero();
};
#endif
