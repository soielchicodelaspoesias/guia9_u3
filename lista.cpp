#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

// funcion para ingresar numeros
void Lista::crear (Numeros *numeros) {
    Nodo *tmp;

    // se crea un nuevo nodo
    tmp = new Nodo;
    tmp->numeros = numeros;
    tmp->sig = NULL;

    //si el es primer nodo agregado de la lista, lo deja como raíz y como último nodo
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    // sino es el primer nodo agregado, apunta el actual último nodo al nuevo y deja el nuevo nodo como el último de la lista
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}
// funcion que verifica si existe una colision
void Lista::agrega_hash_lista_enlazada(Lista *arreglo, int numero){
  // se crean las variables
  int p;
  int contador = 0;
  // se genera una temporal de nodo
  Nodo *tmp = this->raiz;
  // hash para generar la direccion
  p = ((numero)%20);
  // while que recorre la lista enlazada
  while (tmp != NULL) {
    // cuando el contador es igual a p osea en la posicion que se desea ingresar el numero
    if(contador==p){
      // si en la posicion la lista tiene un numero y es distinto de '\0'
      if(tmp->numeros->get_numero() != numero && tmp->numeros->get_numero() != '\0'){
        // hay una colision y se llama a la funcion encadenamiento
        cout << "Colision" << endl;
        encadenamiento(numero);
      }else{
        // sino se agrega el numero en la lista y se imprime la posicion
        tmp->numeros = new Numeros(numero);
        cout << "El numero se encuentra en la posicion: " << p << endl;
      }
    }
    // se le suma al contador y el tmp se hace el siguiente para recorrer la lista
    contador++;
    tmp = tmp->sig;
  }
}
// funcion que soluciona las colisiones
void Lista::encadenamiento(int numero){
  // se cren las variables d, q que es una temporal de nodo un contador y el tmp que es otro temporal
  int d;
  Nodo *q;
  int contador = 0;
  Nodo *tmp = this->raiz;
  // hash para obtener la posicion en la que deberia ir el numero
  d = ((numero)%20);
  // while para recorrer la lista
  while (tmp != NULL) {
    // si el contador es igual a d quiere decir que estamos en el nodo donde debe ir el numero
    if(contador==d){
      // si en arreglo en la posicion d es distinto de '\0' y ademas es igual a l numero que se desea agregar
      if(tmp->numeros->get_numero() == numero &&tmp->numeros->get_numero() != '\0'){
        // se muesta el numero y la posicion
        cout << "El numero esta en la posicion: " << d << endl;
      }else{
        // encambio soluciona la colision
        // q se hace el siguiente nodo
        q = tmp->sig;
        // el ciclo que recorre las siguientes posiciones de la lista hasta encontran una vacia
        while(tmp->numeros->get_numero() != numero &&tmp->numeros->get_numero() != '\0'){
          // por cada repeticion se le suma uno al contador
          contador++;
          // y q se hace el siguiente nodo
          q = q->sig;
          // si el contador pasa de 20 quiere decir que se sale de la lista
          if(contador > 21){
            // se reinician las listas
            contador = 0;
            q = this->raiz;
          }
        }
        // una vez superado el ciclo quiere decir que se encontro una posiciones
        // se iguala tmp a la temporal para agregar el numero en la posicion correspondiente
        tmp = q;
        // se guarda el numero en e nodo y se imprime la posicion
        tmp->numeros = new Numeros(numero);
        cout << "El numero se encuentra en la posicion: " << contador << endl;
      }
    }
    // se le suma a contador
    contador++;
    // el nodo se va haciendo el siguiente
    tmp = tmp->sig;
  }
}

void Lista::imprimir () {
    // se crea una variable temporal
    Nodo *tmp = this->raiz;
    // se recorre la lista hasta que la variable temporal sea nulo
    cout << "Lista: " << endl;
    while (tmp != NULL) {
      // se imprime cada dato
      cout << " |" << tmp->numeros->get_numero() << "|";
      tmp = tmp->sig;
    }
    cout << endl;
}
// funcion que busca
void Lista::buscar() {
  // se crean las variables
  int buscador;
  int contador = 0;
  int condicion = 0;
  // se le pide al usuario el numero por buscar
  cout << "Inserte el numero a buscar: ";
  cin >> buscador;
  // se crea un nodo
  Nodo *tmp = this->raiz;
  // se recorre la lista
  while (tmp != NULL) {
    // si el numero esta en la lista se rompe el ciclo
    if(tmp->numeros->get_numero() == buscador){
      // se le suma uno a condicion
      condicion++;
      break;
    }
    contador ++;
    tmp = tmp->sig;
  }
  // si la condicion es 1 el numero esta en la lista
  if(condicion == 1){
    // se imprime la posicion
    cout << "el numero esta en la posicion: " << contador << endl;
  }else{
    // sino el numero no esta en la lsita
    cout <<"El numero no se encuentra en la lista" << endl;
  }
}
