#include <iostream>
using namespace std;

#ifndef COLISIONADOR_h
#define COLISIONADOR_h
// se crea la clase
class Colisionador {
    private:
    public:
        // se genera el constructor
        Colisionador();
        //funciones
        int hash_normal(int numero);
        int hash_prima(int numero);
        void agrega_hash(int *arreglo, int numero, char letra);
        void imprime(int *arreglo);
        void doble_direccion_hash(int *arreglo, int numero);
        void prueba_cuadratica(int *arreglo, int numero);
        void prueba_lineal(int *arreglo, int numero);


};
#endif
