#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        // metodos privados
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        // constructor
        Lista();
        // funciones
        void crear (Numeros *numeros);
        void imprimir ();
        void encadenamiento(int numero);
        void agrega_hash_lista_enlazada(Lista *arreglo, int numero);
        void buscar();
};
#endif
