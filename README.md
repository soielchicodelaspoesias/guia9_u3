# guia9_u3
*métodos de Busqueda*

***Pre-requisitos***

- C++
- Make
- Ubuntu

***Instalación***

- _Instalar Make._

    abrir terminal y escriba el siguiente codigo:
    $sudo apt install make


***Problematica***

Realizar un programa que almacene numeros en un arreglo y tambien permita buscarlos dentro de este, los numeros tienen que ser ingresador mediante tabla hash, y si hay colisiones solucionarlas mediante los metodos: prueba lineal, prueba cuadratica, doble hash y mediante encadenamiento (listas enlazadas), el metodo a utilizar debe ser escojido por el usuario.

***Ejecutando***

- Para ejecutar el programa debe tener todos los archivos de este repositorio en una misma carpeta, depues en la terminal usar el comando: **$_make_**, esperar que compile y ejecutar el programa con el siguiente comando: **_$./programa_**, ademas debe entregar un parametro por terminal despues del **_$./programa_** separado por un espacio, segun la letra que ingrese es con el metodo que se trabajara
- "L" {Prueba linea}
- "C" {Prueba cuadratica}
- "D" {Doble hash}
- "E" {Encadenamiento}

Un ejemplo seria si quiero trabajar con prueba lineal: _$./programa L

Al ejecutar el programa, independiente de la letra que ingrese, siempre y cuando que este dentro de {L,C,D,E}, se encontrara con un menu donde la primera opcion sera ingresar, en esta le debera ingresar un numero, si al numero se le designa una posicion en la cual ya se encuentra otro numero, se mostrara en pantalla que hubo una colision por lo que se mostrara la nueva posicion del numero y el contenido de la lista, la siguiente opcion es buscar, en esta se le preguntara el numero que desea buscar, si este esta en la lista se imprimira la posicion en la que se encuentra, sino se muentra en pantalla que no se encuentra 

***Construido con C++***

***Librerias***

- Stdlib
- Stdio
- Iostream


***Version 0.1***

***Autor***

Luis Rebolledo



